; (_HOLEGROUP holeOperation panelCode pointList orientType connectorParameters)
; (_GETHOLEPOINTLIST operationType pointParams panelParams fittingParams)
; (_GETHOLEPOINTLIST operationType (list holesCalculationLength constantAxis axisZ offsetingAxis) (list frontStartingLength backStartingLength reverseStartingPoint panelLength) (list fittingParameters shiftOffsetValues)))

(_RUNDEFAULTHELPERRCP "CONNECTORS_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "OPERATIONS_DEFAULT" nil "p2chelper")

(if (_NOTNULL USE_PROJECT_PRICING_RECIPES)
	(progn
		; If project pricing recipes will be used instead of operations then there is no need for notch operations also
		(_NONOTCH)
	)
	(progn
		; According to position of bottom nailer side panel holes are calibrated 
		(if (_EXISTPANEL BOTTOM_NAILER_OP_FACE)
			(progn
			
				(_FSET (_ 'lengthParametersForBottomNailerAndLeft (_CALCULATEPANELSINTERSECTIONLENGTH BOTTOM_NAILER_HEI (_ 0 0 0 0))))
				(_FSET (_ 'lengthForBottomNailerAndLeft (car lengthParametersForBottomNailerAndLeft)))

				(_FSET (_ 'lengthParametersForBottomNailerAndRight (_CALCULATEPANELSINTERSECTIONLENGTH BOTTOM_NAILER_HEI (_ 0 0 0 0))))
				(_FSET (_ 'lengthForBottomNailerAndRight (car lengthParametersForBottomNailerAndRight)))
				
				(_FSET (_ 'index 0))
				(foreach operationParams OPERATION_LIST
					(mapcar '_SETA '(operationName connectorParams horizontalHolePos) operationParams)

					(_FSET (_ 'bottomNailerPointListForLeftSide (_GETHOLEPOINTLIST operationName 
																	(_ lengthForBottomNailerAndLeft 0 (car horizontalHolePos) "Y") 
																	(_ 0 0 nil BOTTOM_NAILER_HEI) 
																	(_ FITTING_PARAMETERS nil))))
					(_HOLEGROUP operationName BOTTOM_NAILER_OP_FACE bottomNailerPointListForLeftSide "X+" connectorParams)
					
					(_FSET (_ 'bottomNailerPointListForRightSide (_GETHOLEPOINTLIST operationName 
																	(_ lengthForBottomNailerAndRight BOTTOM_NAILER_WID (car horizontalHolePos) "Y") 
																	(_ 0 0 nil BOTTOM_NAILER_HEI) 
																	(_ FITTING_PARAMETERS nil))))
					(_HOLEGROUP operationName BOTTOM_NAILER_OP_FACE bottomNailerPointListForRightSide "X-" connectorParams)
					
					; Bottom nailer bottom panel middle hole operation
					(if (and (equal IS_BOTTOM_NAILER_BOTTOM_PANEL_HOLE_AVAILABLE T) (equal operationName WHICH_CONN_IS_DOMINANT))
						(cond 
							((equal operationName "MINIFIX_OPERATION")
								(_MINIFIX "MIDDLE" BOTTOM_NAILER_OP_FACE (_ (_ (/ BOTTOM_NAILER_WID 2) 0 (* -1 (/ __AD_PANELTHICK 2))) MINIFIX_PARAMETERS) "Y+")
								(_MINIFIX "MIDDLE" BOTTOM_PANEL_OP_FACE (_ (_ (- BOTTOM_PANEL_HEI (/ __AD_PANELTHICK 2)) (+ BOTTOM_PANEL_RIGHT_VARIANCE bottomSideStyleV1 (/ BOTTOM_NAILER_WID 2)) 0) MINIFIX_PARAMETERS) nil)
							)
							((equal operationName "DOWEL_OPERATION")
								(_DOWEL "MIDDLE" BOTTOM_NAILER_OP_FACE (_ (_ (/ BOTTOM_NAILER_WID 2) 0 (* -1 (/ __AD_PANELTHICK 2))) DOWEL_PARAMETERS) "Y+")
								(_DOWEL "MIDDLE" BOTTOM_PANEL_OP_FACE (_ (_ (- BOTTOM_PANEL_HEI (/ __AD_PANELTHICK 2)) (+ BOTTOM_PANEL_RIGHT_VARIANCE bottomSideStyleV1 (/ BOTTOM_NAILER_WID 2)) 0) DOWEL_PARAMETERS) nil)
							)
							((equal operationName "RAFIX_OPERATION")
								(_RAFIXMAIN "MIDDLE" BOTTOM_NAILER_OP_FACE (_ (_ (/ BOTTOM_NAILER_WID 2) 0 (* -1 (/ __AD_PANELTHICK 2))) RAFIX_PARAMETERS) "Y+")
								(_RAFIXMAIN "MIDDLE" BOTTOM_PANEL_OP_FACE (_ (_ (- BOTTOM_PANEL_HEI (/ __AD_PANELTHICK 2)) (+ BOTTOM_PANEL_RIGHT_VARIANCE bottomSideStyleV1 (/ BOTTOM_NAILER_WID 2)) 0) RAFIX_PARAMETERS) nil)
							)
							((equal operationName "SCREW_OPERATION")
								(_SCREW "MIDDLE" BOTTOM_NAILER_OP_FACE (_ (_ (/ BOTTOM_NAILER_WID 2) 0 (* -1 (/ __AD_PANELTHICK 2))) SCREW_PARAMETERS) "Y+")
								(_SCREW "MIDDLE" BOTTOM_PANEL_OP_FACE (_ (_ (- BOTTOM_PANEL_HEI (/ __AD_PANELTHICK 2)) (+ BOTTOM_PANEL_RIGHT_VARIANCE bottomSideStyleV1 (/ BOTTOM_NAILER_WID 2)) 0) SCREW_PARAMETERS) nil)
							)
							((equal operationName "CABINEO_OPERATION")
								(_CABINEOMAIN "MIDDLE" BOTTOM_NAILER_OP_FACE (_ (_ (/ BOTTOM_NAILER_WID 2) 0 (* -1 (/ __AD_PANELTHICK 2))) CABINEO_PARAMETERS) "Y+")
								(_CABINEOMAIN "MIDDLE" BOTTOM_PANEL_OP_FACE (_ (_ (- BOTTOM_PANEL_HEI (/ __AD_PANELTHICK 2)) (+ BOTTOM_PANEL_RIGHT_VARIANCE bottomSideStyleV1 (/ BOTTOM_NAILER_WID 2)) 0) CABINEO_PARAMETERS) nil)
							)
						)
					)

					(if (_EXISTPANEL LEFT_PANEL_CODE)
						(progn
							(_FSET (_ 'leftPanelPointListForBottomNailer (_GETHOLEPOINTLIST operationName 
																			(_ lengthForBottomNailerAndLeft (- (+ LEFT_PANEL_WID BOTTOM_PANEL_BACK_VARIANCE) LEFT_PANEL_BACK_VARIANCE OFFSET_FOR_OPPOSITE_OF_HOR_HOLES (cadr horizontalHolePos)) 0 "Y") 
																			(_ LEFT_PANEL_LOWER_VARIANCE 0 nil BOTTOM_NAILER_HEI) 
																			(_ FITTING_PARAMETERS nil))))
							(_HOLEGROUP operationName LEFT_PANEL_CODE leftPanelPointListForBottomNailer nil connectorParams)
						)
					)
					(if (_EXISTPANEL RIGHT_PANEL_CODE)
						(progn
							(_FSET (_ 'rightPanelPointListForBottomPanel (_GETHOLEPOINTLIST operationName 
																			(_ lengthForBottomNailerAndRight (+ (- RIGHT_PANEL_BACK_VARIANCE BOTTOM_PANEL_BACK_VARIANCE) OFFSET_FOR_OPPOSITE_OF_HOR_HOLES (cadr horizontalHolePos)) 0 "Y") 
																			(_ RIGHT_PANEL_LOWER_VARIANCE 0 nil BOTTOM_NAILER_HEI) 
																			(_ FITTING_PARAMETERS nil))))
							(_HOLEGROUP operationName RIGHT_PANEL_CODE rightPanelPointListForBottomPanel nil connectorParams)
						)
					)
					
					(_FSET (_ 'index (+ index 1)))
				)
			)
		)
	)
)

