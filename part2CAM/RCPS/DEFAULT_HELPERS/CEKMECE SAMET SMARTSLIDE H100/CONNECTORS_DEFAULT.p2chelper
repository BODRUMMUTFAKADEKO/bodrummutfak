MINIFIX_OPERATION -> "Minifix operation"
DOWEL_OPERATION -> "Dowel operation"
RAFIX_OPERATION -> "Rafix operation"
SCREW_OPERATION -> "Screw operation"
CABINEO_OPERATION -> "Cabineo operation"
QFIX_OPERATION -> "Qfix operation"

FITTING_TYPE -> "Holes calculation type:  TYPE_1 -> Formulation 1; TYPE_2 -> Formulation 2; TYPE_3 -> Constant Values"
WHICH_CONN_IS_DOMINANT -> "Dominant connector name when using more than one connector"

DISTANCE_TO_ADD_HOLE -> "Minimum distance to add another hole for formulation types"
HOLE_STEP -> "Minimum distance or step factor between holes"
HOLE_FRONT_OFFSET -> "Hole front offset"
HOLE_BACK_OFFSET -> "Hole back offset"

T3_PRIMARY_FRONT_OFFSET -> "Primary connector hole front offset for TYPE_3"
T3_PRIMARY_BACK_OFFSET -> "Primary connector hole back offset for TYPE_3"
T3_PRIMARY_LIMIT_FOR_MIDDLE_CONNECTOR -> "Minimum length limit for Primary connector middle hole for TYPE_3"
T3_PRIMARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER -> "Primary connector middle hole offset from panel midpoint for TYPE_3"
T3_PRIMARY_IS_MIDDLE_CONNECTOR_AVAILABLE -> "Is primary connector middle hole available?"

T3_SECONDARY_FRONT_OFFSET -> "Secondary connector hole front offset for TYPE_3"
T3_SECONDARY_BACK_OFFSET -> "Secondary connector hole back offset for TYPE_3"
T3_SECONDARY_LIMIT_FOR_MIDDLE_CONNECTOR -> "Minimum length limit for secondary connector middle hole for TYPE_3"
T3_SECONDARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER -> "Secondary connector middle hole offset from panel midpoint for TYPE_3"
T3_SECONDARY_IS_MIDDLE_CONNECTOR_AVAILABLE -> "Is secondary connector middle hole available?"

SHIFTING_VALUE_FOR_OFFSETS -> "Offset between primary and secondary connector"

LIMIT_FOR_NO_OPERATION -> "Length limit for not doing operation"

LIMIT_FOR_SMALL_PARTS -> "Maximum length limit for small parts"

SP_PRIMARY_HOLE_ADDING_TYPE -> "Primary connector hole type for small parts: \"ONE_HOLE\" or \"TWO_HOLE\""
SP_PRIMARY_DISTANCE_FROM_CENTER -> "Primary connector offset for \"ONE_HOLE\" type from center of small parts"
SP_PRIMARY_FRONT_OFFSET -> "Primary connector front offset for small parts"
SP_PRIMARY_BACK_OFFSET -> "Primary connector back offset for small parts"

SP_SECONDARY_HOLE_ADDING_TYPE -> "Secondary connector hole type for small parts: \"ONE_HOLE\" or \"TWO_HOLE\""
SP_SECONDARY_DISTANCE_FROM_CENTER -> "Secondary connector offset for \"ONE_HOLE\" type from center of small parts"
SP_SECONDARY_FRONT_OFFSET -> "Secondary connector front offset for small parts"
SP_SECONDARY_BACK_OFFSET -> "Secondary connector back offset for small parts"

OFFSET_FOR_OPPOSITE_OF_HOR_HOLES -> "Sliding value for opposite of horizontal holes"

DO_NOT_ADD_CONN_AS_ITEM -> "Do not add connectors as item to requirement list"
		
;prefvars
; (if (null CONNECTORS_DEFAULT_LOADED)
	; (progn
		(_SETA 'MINIFIX_OPERATION nil)
		(_SETA 'DOWEL_OPERATION nil)
		(_SETA 'RAFIX_OPERATION nil)
		(_SETA 'SCREW_OPERATION T)
		(_SETA 'CABINEO_OPERATION nil)
		(_SETA 'QFIX_OPERATION nil)
		
		; Operation types and values
		; TYPE_1 -> Formulation 1 (Constant step x factor, variable offset)
		; TYPE_2 -> Formulation 2 (Variable step, constant offset)
		; TYPE_3 -> Constant Values (Constant offsets)
		(_SETA 'FITTING_TYPE "TYPE_3")
	
		(cond
			((and (_NOTNULL MINIFIX_OPERATION) (null DOWEL_OPERATION) (null RAFIX_OPERATION) (null SCREW_OPERATION) (null CABINEO_OPERATION) (null QFIX_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "MINIFIX_OPERATION"))
			)
			((and (_NOTNULL DOWEL_OPERATION) (null MINIFIX_OPERATION) (null RAFIX_OPERATION) (null SCREW_OPERATION) (null CABINEO_OPERATION) (null QFIX_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "DOWEL_OPERATION"))
			)
			((and (_NOTNULL RAFIX_OPERATION) (null MINIFIX_OPERATION) (null DOWEL_OPERATION) (null SCREW_OPERATION) (null CABINEO_OPERATION) (null QFIX_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "RAFIX_OPERATION"))
			)
			((and (_NOTNULL SCREW_OPERATION) (null MINIFIX_OPERATION) (null DOWEL_OPERATION) (null RAFIX_OPERATION) (null CABINEO_OPERATION) (null QFIX_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "SCREW_OPERATION"))
			)
			((and (_NOTNULL CABINEO_OPERATION) (null MINIFIX_OPERATION) (null DOWEL_OPERATION) (null SCREW_OPERATION) (null RAFIX_OPERATION) (null QFIX_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "CABINEO_OPERATION"))
			)
			((and (_NOTNULL QFIX_OPERATION) (null MINIFIX_OPERATION) (null DOWEL_OPERATION) (null SCREW_OPERATION) (null RAFIX_OPERATION) (null CABINEO_OPERATION))
				(_FSET (_ 'WHICH_CONN_IS_DOMINANT "QFIX_OPERATION"))
			)
			(T
				(_SETA 'WHICH_CONN_IS_DOMINANT "SCREW_OPERATION")
			)
		)
		
		; TYPE_1 and TYPE_2 - Formulation parameters
		(_SETA 'DISTANCE_TO_ADD_HOLE 200)
		(_SETA 'HOLE_STEP 32)
		(_SETA 'HOLE_FRONT_OFFSET 50)
		(_SETA 'HOLE_BACK_OFFSET 50)
		
		; TYPE_3 parameters
		(_SETA 'T3_PRIMARY_FRONT_OFFSET 50)
		(_SETA 'T3_PRIMARY_BACK_OFFSET 50)		
		(_SETA 'T3_PRIMARY_LIMIT_FOR_MIDDLE_CONNECTOR 6000)
		(_SETA 'T3_PRIMARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER 0)
		(_SETA 'T3_PRIMARY_IS_MIDDLE_CONNECTOR_AVAILABLE T)

		(_SETA 'T3_SECONDARY_FRONT_OFFSET 82)
		(_SETA 'T3_SECONDARY_BACK_OFFSET 82)
		(_SETA 'T3_SECONDARY_LIMIT_FOR_MIDDLE_CONNECTOR 400)
		(_SETA 'T3_SECONDARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER 0)
		(_SETA 'T3_SECONDARY_IS_MIDDLE_CONNECTOR_AVAILABLE nil)
		
		(_SETA 'SHIFTING_VALUE_FOR_OFFSETS 20)
		
		(_SETA 'LIMIT_FOR_NO_OPERATION 60)
		; Small part parameters
		(_SETA 'LIMIT_FOR_SMALL_PARTS 101)
		; Primary connector parameters for small parts
		(_SETA 'SP_PRIMARY_HOLE_ADDING_TYPE "ONE_HOLE")
		(_SETA 'SP_PRIMARY_DISTANCE_FROM_CENTER 0)
		(_SETA 'SP_PRIMARY_FRONT_OFFSET 10)
		(_SETA 'SP_PRIMARY_BACK_OFFSET 30)
		; Secondary connector parameters for small parts
		(_SETA 'SP_SECONDARY_HOLE_ADDING_TYPE "TWO_HOLE")
		(_SETA 'SP_SECONDARY_DISTANCE_FROM_CENTER -32)
		(_SETA 'SP_SECONDARY_FRONT_OFFSET 18)
		(_SETA 'SP_SECONDARY_BACK_OFFSET 18)
		
		; Sliding value for opposite of horizontal holes
		(_SETA 'OFFSET_FOR_OPPOSITE_OF_HOR_HOLES 0)
		
		(_SETA 'DO_NOT_ADD_CONN_AS_ITEM nil)
			
		; CONTINGENCIES
		(_FSET (_ 'OPERATION_LIST nil))

		(if (equal MINIFIX_OPERATION T)	
			(progn 
				(_RUNDEFAULTHELPERRCP "MINIFIX_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "MINIFIX_OPERATION" (append MINIFIX_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ MINIFIX_HOR_POS MINIFIX_VER_POS)) OPERATION_LIST)))
			)
		)
		(if (equal DOWEL_OPERATION T) 
			(progn 
				(_RUNDEFAULTHELPERRCP "DOWEL_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "DOWEL_OPERATION" (append DOWEL_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ DOWEL_HOR_POS DOWEL_VER_POS)) OPERATION_LIST)))
			)
		)
		(if (equal RAFIX_OPERATION T) 
			(progn 
				(_RUNDEFAULTHELPERRCP "RAFIX_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "RAFIX_OPERATION" (append RAFIX_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ 0 RAFIX_BOLT_HOLE_POS)) OPERATION_LIST)))
			)
		)
		(if (equal SCREW_OPERATION T) 
			(progn 
				(_RUNDEFAULTHELPERRCP "SCREW_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "SCREW_OPERATION" (append SCREW_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ SCREW_HOR_POS SCREW_VER_POS)) OPERATION_LIST)))
			)
		)
		(if (equal CABINEO_OPERATION T)	
			(progn 
				(_RUNDEFAULTHELPERRCP "CABINEO_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "CABINEO_OPERATION" (append CABINEO_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ 0 CABINEO_BOLT_HOLE_POS)) OPERATION_LIST)))
			)
		)
		(if (equal QFIX_OPERATION T)	
			(progn 
				(_RUNDEFAULTHELPERRCP "QFIX_DEFAULT" nil "p2chelper") 
				(_FSET (_ 'OPERATION_LIST (cons (_ "QFIX_OPERATION" (append QFIX_PARAMETERS (list DO_NOT_ADD_CONN_AS_ITEM)) (_ 0 QFIX_BOLT_HOLE_POS)) OPERATION_LIST)))
			)
		)
		
		; All fitting parameters set to a list
		(_FSET (_ 'FITTING_PARAMETERS (_ FITTING_TYPE WHICH_CONN_IS_DOMINANT (_ DISTANCE_TO_ADD_HOLE HOLE_STEP HOLE_FRONT_OFFSET HOLE_BACK_OFFSET) (_ (_ T3_PRIMARY_FRONT_OFFSET T3_PRIMARY_BACK_OFFSET T3_PRIMARY_LIMIT_FOR_MIDDLE_CONNECTOR T3_PRIMARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER T3_PRIMARY_IS_MIDDLE_CONNECTOR_AVAILABLE) (_ T3_SECONDARY_FRONT_OFFSET T3_SECONDARY_BACK_OFFSET T3_SECONDARY_LIMIT_FOR_MIDDLE_CONNECTOR T3_SECONDARY_MIDDLE_CONNECTOR_OFFSET_FROM_CENTER T3_SECONDARY_IS_MIDDLE_CONNECTOR_AVAILABLE) SHIFTING_VALUE_FOR_OFFSETS) (_ LIMIT_FOR_SMALL_PARTS SP_PRIMARY_HOLE_ADDING_TYPE SP_PRIMARY_DISTANCE_FROM_CENTER SP_PRIMARY_FRONT_OFFSET SP_PRIMARY_BACK_OFFSET SP_SECONDARY_HOLE_ADDING_TYPE SP_SECONDARY_DISTANCE_FROM_CENTER SP_SECONDARY_FRONT_OFFSET SP_SECONDARY_BACK_OFFSET) LIMIT_FOR_NO_OPERATION)))
		
		; In any cases, PANELS_JOINT_TYPE_DEFAULT must be called before/after OPERATION_DEFAULT; so to make it easy we call it here
		(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
		(_FSET (_ 'CONNECTORS_DEFAULT_LOADED T))
	; )
; )

; Standard Values, USE_PROJECT_PRICING_RECIPES nil
(_NONOTCH)
