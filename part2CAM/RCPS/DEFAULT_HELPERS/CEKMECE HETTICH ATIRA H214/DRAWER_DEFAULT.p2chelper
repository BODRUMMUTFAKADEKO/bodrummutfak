; ---------------------------------------------------------------------------------------------------------
; Drawer Body
USE_DOOR_AS_DRAWER_FRONT_PANEL -> "Use drawer door as drawer front panel"
DRAWER_CABIN_ELEV -> "Distance from bottom edge of drawer door to bottom of drawer"
CHANGE_DRAWER_WITH_BOUNDRIES -> "According to boundries, depth of drawer changes"
DRAWER_DEPTH_UPPERBOUND -> "Uppermost boundry for depth of drawer"

DRAWER_TOTAL_GAP_WID -> "Total drawer rail operating range"
DRAWER_TOTAL_GAP_DEP -> "Minimum clearance between module depth and drawer rail"
GAP_BETWEEN_DRAWER_AND_RAIL -> "Clearance between drawer depth and drawer rail"
DRAWER_BODY_HEI -> "Height of drawer body panels; back, front and sides"
DRAWER_PANELS_WID_DIFF -> "Difference between drawer bottom panel width and drawer back-front panel width"
DRAWER_PANELS_HEI_DIFF_TOP -> "Difference between drawer side panels height and drawer back-front panel height on topside"
DRAWER_PANELS_HEI_DIFF_BOTTOM -> "Difference between drawer side panels height and drawer back-front panel height on bottomside"

SINKU_WID_OF_DRAWER -> "Width of notched part on sink unit drawer bottom panel"
SINKU_DEP_OF_DRAWER -> "Height of notched part on sink unit drawer bottom panel"

DRAWER_BODY_MAT -> "Material of drawer body panels; back, front and sides"
DRAWER_BODY_THICKNESS -> "Thickness of drawer body panels; back, front and sides"
DRAWER_BOTTOM_PANEL_MAT -> "Material of drawer bottom panel"
DRAWER_BOTTOM_PANEL_THICKNESS -> "Thickness of drawer bottom panel"
DRAWER_BOTTOM_PANEL_EXTRA_WIDTH_VARIANCE -> "Extra width variance for drawer bottom panel"
DRAWER_BOTTOM_PANEL_EXTRA_DEPTH_VARIANCE -> "Extra depth variance for drawer bottom panel"

; Drawer Body Assembly parameters
GROOVE_MODE_FOR_BODY_ASSEMBLY -> "Mode of drawer body assembly with groove. Modes are ``WID``,``HEI``,``BOTH``"
PERFORM_GROOVE_ON_PANELS -> "Perform groove operation on drawer panels"
RAIL_DOOR_GROOVE_BOTTOM_OFFSET_VALUE -> "Groove offset distance to bottom of door"

DBA_GROOVE_DISTANCE -> "Groove distance of drawer body assembly"
DBA_GROOVE_WID -> "Groove width of drawer body assembly"
DBA_GROOVE_DEPTH -> "Groove depth of drawer body assembly"

DRAWER_PANELS_THINNING_OP_TYPE -> "Operation type of drawer panels thinning. Modes are ``POCKETING`` , ``MILLING``"
THINNING_OVERFLOW_VALUE -> "Value of begining of thinning operation from outside"
THINNING_WIDTH_BOTTOM -> "Width of thinning for left and right edges of drawer bottom panel"
THINNING_DEPTH_BOTTOM -> "Depth of thinning for left and right edges of drawer bottom panel"
THINNING_WIDTH_BACK -> "Width of thinning for left and right edges of drawer back panel"
THINNING_DEPTH_BACK -> "Depth of thinning for left and right edges of drawer back panel"

RAIL_BORDURE_CODE -> "Rail bordure code"
IS_RAIL_BORDURE_AVAILABLE -> "Is there bordure in rail package"
RAIL_BACK_PANEL_HOLDER_CODE -> "Rail back panel holder code"
IS_RAIL_BACK_PANEL_HOLDER_AVAILABLE -> "Is there back panel holder in rail package?"
; ---------------------------------------------------------------------------------------------------------
; Drawer Door Holes
DRAWER_DOOR_HOLES_OFFSET -> "Offset from drawer door holes to vertical edges of drawer door"
DRAWER_DOOR_FIRST_HOLE_DISTANCE -> "Distance from first drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_SECOND_HOLE_DISTANCE -> "Distance from second drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_THIRD_HOLE_DISTANCE -> "Distance from third drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_HOLES_DIAMETER -> "Diameter of drawer door holes"
DRAWER_DOOR_HOLES_DEPTH -> "Depth of drawer door holes"
DRAWER_DOOR_EXTRA_HOLES_NUMBER -> "Number of drawer door extra holes"
DRAWER_DOOR_FIRST_EXTRA_HOLE_DISTANCE -> "Distance from first extra drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_SECOND_EXTRA_HOLE_DISTANCE -> "Distance from second extra drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_EXTRA_HOLES_DIAMETER -> "Diameter of drawer door extra holes"
DRAWER_DOOR_EXTRA_HOLES_DEPTH -> "Depth of drawer door extra holes"
DRAWER_DOOR_HEI_REQUIRED_FOR_THIRD_HOLE -> "Required door height to perform third drawer door hole"

HDRAWER_DOOR_CONN_HOLE_DIAMETER -> "Diameter of drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_HOLE_DEPTH -> "Depth of drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_HOLE_DISTANCE -> "Distance from topside of drawer door to drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_MECHANISM_CODE -> "Code of drawer door-hidden drawer connection mechanism"
; ---------------------------------------------------------------------------------------------------------
; Drawer Rail Holes
RAIL_CODE -> "Code of drawer rail"
RAIL_HOLES_DIAMETER -> "Diameter of drawer rail holes"
RAIL_HOLES_DEPTH -> "Depth of drawer rail holes"

RAIL_LENGTH_AND_COORDINATE_LIST -> "List of drawer rail holes and coordinates for rail packages. Format: (_ (_ Rail_length (_ Rail_hole1 Rail_hole2 ...)) ...) Ex: (_ (_ 300 (_ (_ 37 60 0) (_ 293 60 0))) ...)"

;prefvars
;(if (null DRAWER_DEFAULT_LOADED)
;	(progn
		; ---------------------------------------------------------------------------------------------------------
		; Drawer-module related parameters
		(_SETA 'USE_DOOR_AS_DRAWER_FRONT_PANEL nil)
		(_SETA 'DRAWER_CABIN_ELEV 20)
		(_SETA 'CHANGE_DRAWER_WITH_BOUNDRIES nil)
		(_SETA 'DRAWER_DEPTH_UPPERBOUND 500)
		; Drawer body parameters
		(_SETA 'DRAWER_TOTAL_GAP_WID 83)
		(_SETA 'DRAWER_TOTAL_GAP_DEP 50)
		(_SETA 'GAP_BETWEEN_DRAWER_AND_RAIL 1)
		(_SETA 'DRAWER_BODY_HEI 214)
		(_SETA 'DRAWER_PANELS_WID_DIFF 10)
		(_SETA 'DRAWER_PANELS_HEI_DIFF_TOP 0)
		(_SETA 'DRAWER_PANELS_HEI_DIFF_BOTTOM 0)
		; Sink unit drawer parameters
		(_SETA 'SINKU_WID_OF_DRAWER 598)
		(_SETA 'SINKU_DEP_OF_DRAWER 284)
		; Material and thickness parameters of drawer panels
		(_SETA 'DRAWER_BODY_MAT __MODULBODYMAT)
		(_SETA 'DRAWER_BODY_THICKNESS __AD_PANELTHICK)
		(_SETA 'DRAWER_BOTTOM_PANEL_MAT __MODULBODYMAT)
		(_SETA 'DRAWER_BOTTOM_PANEL_THICKNESS __AD_PANELTHICK)
		(_SETA 'DRAWER_BOTTOM_PANEL_EXTRA_WIDTH_VARIANCE 0.0)
		(_SETA 'DRAWER_BOTTOM_PANEL_EXTRA_DEPTH_VARIANCE 0.0)
		(_SETA 'GROOVE_MODE_FOR_BODY_ASSEMBLY "BOTH")

		(_SETA 'PERFORM_GROOVE_ON_PANELS T)
		(_SETA 'RAIL_DOOR_GROOVE_BOTTOM_OFFSET_VALUE 0)

		(_SETA 'DBA_GROOVE_DISTANCE 12)
		(_SETA 'DBA_GROOVE_WID 8)
		(_SETA 'DBA_GROOVE_DEPTH 8)

		(_SETA 'DRAWER_PANELS_THINNING_OP_TYPE "POCKETING")
		(_SETA 'THINNING_OVERFLOW_VALUE 10)
		(_SETA 'THINNING_WIDTH_BOTTOM 10)
		(_SETA 'THINNING_DEPTH_BOTTOM 2)
		(_SETA 'THINNING_WIDTH_BACK 10)
		(_SETA 'THINNING_DEPTH_BACK 2)

		(_SETA 'RAIL_BORDURE_CODE "BORDURE")
		(_SETA 'IS_RAIL_BORDURE_AVAILABLE nil)
		(_SETA 'RAIL_BACK_PANEL_HOLDER_CODE "HOLDER")
		(_SETA 'IS_RAIL_BACK_PANEL_HOLDER_AVAILABLE nil)
		; ---------------------------------------------------------------------------------------------------------		
		; Drawer door holes parameters
		(_SETA 'DRAWER_DOOR_HOLES_OFFSET 32)
		(_SETA 'DRAWER_DOOR_FIRST_HOLE_DISTANCE 72)
		(_SETA 'DRAWER_DOOR_SECOND_HOLE_DISTANCE 104)
		(_SETA 'DRAWER_DOOR_THIRD_HOLE_DISTANCE 232)
		(_SETA 'DRAWER_DOOR_HOLES_DIAMETER 5)
		(_SETA 'DRAWER_DOOR_HOLES_DEPTH 14)
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_NUMBER 0)
		(_SETA 'DRAWER_DOOR_FIRST_EXTRA_HOLE_DISTANCE 123)
		(_SETA 'DRAWER_DOOR_SECOND_EXTRA_HOLE_DISTANCE 139)
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_DIAMETER 3)
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_DEPTH 12.5)
		(_SETA 'DRAWER_DOOR_HEI_REQUIRED_FOR_THIRD_HOLE 250)
		
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DIAMETER 25)
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DEPTH 10)
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DISTANCE 17)
		(_SETA 'HDRAWER_DOOR_CONN_MECHANISM_CODE "DRAWER_DOOR_HIDDEN_DRAWER_CONNECTION_MECHANISM")
		; ---------------------------------------------------------------------------------------------------------
		; Drawer rail holes parameters
		(_SETA 'RAIL_CODE "HETTICH_ATIRA")
		(_SETA 'RAIL_HOLES_DIAMETER 3.0)
		(_SETA 'RAIL_HOLES_DEPTH 2)
		
		; List of drawer rail holes and coordinates for rail packages. Format: (_ (_ Rail_length (_ Rail_hole1 Rail_hole2 ...)) ...) Ex: (_ (_ 300 (_ (_ 37 60 0) (_ 293 60 0))) ...)"
		(_SETA 'RAIL_LENGTH_AND_COORDINATE_LIST (_ (_ 300 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 350 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 400 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 450 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 500 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 550 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 600 (_ (_ 37 60 0) (_ 293 60 0))) 
													(_ 650 (_ (_ 37 60 0) (_ 293 60 0)))))
		
		
		; Creating RAIL_HOLES_FOR_RAIL_PACKAGES list according to RAIL_LENGTH_AND_COORDINATE_LIST
		(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES nil))
		(foreach iLengthAndCoordinate RAIL_LENGTH_AND_COORDINATE_LIST
			(_FSET (_ 'RAIL_HOLES_LIST nil))
			(foreach iCoordinate (cadr iLengthAndCoordinate)
				(_FSET (_ 'RAIL_HOLES_LIST (cons (_ RAIL_HOLES_DIAMETER RAIL_HOLES_DEPTH iCoordinate) RAIL_HOLES_LIST)))
			)
			(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES (cons (append (_ (car iLengthAndCoordinate) (_& (_ RAIL_CODE "_" (convertStr (car iLengthAndCoordinate))))) (reverse RAIL_HOLES_LIST)) RAIL_HOLES_FOR_RAIL_PACKAGES)))
		)		
		(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES (reverse RAIL_HOLES_FOR_RAIL_PACKAGES)))
		
		; ---------------------------------------------------------------------------------------------------------
		; CONTINGENCIES
		; USE_DOOR_AS_DRAWER_FRONT_PANEL
		(if (null (member USE_DOOR_AS_DRAWER_FRONT_PANEL (_ nil T))) (_FSET (_ 'USE_DOOR_AS_DRAWER_FRONT_PANEL T)))
		; CHANGE_DRAWER_WITH_BOUNDRIES
		(if (null (member CHANGE_DRAWER_WITH_BOUNDRIES (_ nil T))) (_FSET (_ 'CHANGE_DRAWER_WITH_BOUNDRIES nil)))
		; HIDDEN DRAWER CONNECTOR MECHANISM UNIT
		(if (null HDRAWER_DOOR_CONN_MECHANISM_UNIT) (_FSET (_ 'HDRAWER_DOOR_CONN_MECHANISM_UNIT "pc")))
		
		; (_FSET (_ 'DRAWER_DEFAULT_LOADED T))
	; )
; )

; PRELIMINARY CONTROLS
; (if (null DRAWER_BODY_PRELIMINARY_CONTROLS)
	; (progn
		(if (null CHANGE_DRAWER_WITH_BOUNDRIES)
			(_FSET (_ 'DRAWER_DEPTH (- __DEP DRAWER_TOTAL_GAP_DEP)))	; Height of panel directly relates to module depth, not boundries
			(_FSET (_ 'DRAWER_DEPTH DRAWER_DEPTH_UPPERBOUND)) ; Boundry for actual module calculated here. Via this boundry, drawer bottom panels height will be calculated
		)
		
		; According to the notch type, possible maximum drawer depth and actualDrawerDepth are calculated
		(if (or (equal __NOTCHTYPE 0) (equal __NOTCHTYPE 4))
			(progn
				(_FSET (_ 'maxDrawerDepthPossible __DEP))
				(_FSET (_ 'actualDrawerDepth DRAWER_DEPTH))
			)
			(progn
				(_FSET (_ 'maxDrawerDepthPossible (- __DEP __NOTCHDIM2)))
				(_FSET (_ 'actualDrawerDepth (- DRAWER_DEPTH __NOTCHDIM2)))
			)
		)
		(_FSET (_ 'paramBody (_& (_ "CDOOR_" __CURDIVORDER "_1_"))))
		(_FSET (_ 'currentDoorELEV (_S2V (_& (_ paramBody "ELEV")))))
		; General validation control for RAIL_HOLES_FOR_RAIL_PACKAGES
		(if (and (_NOTNULL RAIL_HOLES_FOR_RAIL_PACKAGES) (equal (type RAIL_HOLES_FOR_RAIL_PACKAGES) 'LIST))
			(progn
				; LIMIT CONTROLS
				(_FSET (_ 'loopControl nil))
				(_FSET (_ 'numberOfPackages (length RAIL_HOLES_FOR_RAIL_PACKAGES)))
				(_FSET (_ 'packageCounter 1))
				(_FSET (_ 'limitOfRail 0))
				(_FSET (_ 'indexOfRail nil))
				(while (and (null loopControl) (< packageCounter (+ numberOfPackages 1)))
					(_FSET (_ 'packageIndex (- packageCounter 1)))
					(_FSET (_ 'currentPackage (getnth packageIndex RAIL_HOLES_FOR_RAIL_PACKAGES)))
					(_FSET (_ 'currentPackLimit (getnth 0 currentPackage)))
					(cond
						((equal actualDrawerDepth currentPackLimit)
							; No need for further investigation. We are sure this is the package we need
							(_FSET (_ 'loopControl T))
							(_FSET (_ 'limitOfRail currentPackLimit))
							(_FSET (_ 'indexOfRail packageIndex))
						)
						((> actualDrawerDepth currentPackLimit)
							; It is a valid package but is it the best choice for us???
							(if (> currentPackLimit limitOfRail)
								(progn
									(_FSET (_ 'limitOfRail currentPackLimit))
									(_FSET (_ 'indexOfRail packageIndex))
								)
							)
						)
					)
					(_FSET (_ 'packageCounter (+ 1 packageCounter)))
				)
				
				(if (_NOTNULL indexOfRail)
					(_FSET (_ 'activeRailPackage (getnth indexOfRail RAIL_HOLES_FOR_RAIL_PACKAGES)))
				)
			)
		)
		
;		(_FSET (_ 'DRAWER_BODY_PRELIMINARY_CONTROLS T))
;	)
;)
(_NONOTCH)

; IMPORTANT INFORMATION
; Format of RAIL_HOLES_FOR_RAIL_PACKAGES

; Format of RAIL_HOLES_FOR_RAIL_PACKAGES list 	-> 		list RAIL_PACKAGE_1 RAIL_PACKAGE_2 ....
; Format of RAIL_PACKAGE_1 list 			  	->		list upperLimitForRail RAIL_PACKAGE_CODE firstHoleParams secondHoleParams ....
; Format of firstHoleParams list			  	->		list holeDiameter holeDepth holePosition
; Format of holePosition list				  	->		list positionOnAxisX positionOnAxisY positionOnAxisZ
