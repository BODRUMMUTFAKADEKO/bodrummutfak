IS_EXTRA_PART_AVAILABLE -> "Extra part is available?"
IS_BLIND_DOOR_AVAILABLE -> "Is blind door available?"
EXTRA_PART_WID -> "Width of extra part"
IS_BLIND_DOOR_INSIDE_MODULE -> "Is blind door placed inside of module?"
OVERLAPPING_DISTANCE -> "Overlapping distance between extra part and blind door"
ASSUME_DRAWER_AS_DOOR -> "Assume drawer as door"
AKK_PANELS_SOURCE -> "When blind corner panels such as blind door, filler and extra part are made from door material, the source of this panels; INSOURCE or OUTSOURCE"

;AKK fixed and adjustable shelf parameters
BLIND_CORNER_FIXED_SHELF_ELEV_LIST -> "Blind corner fixed shelf elevation list"
BLIND_CORNER_ADJUSTABLE_SHELF_ELEV_LIST -> "Blind corner adjustable shelf elevation list"

; Blind Door Parameters
BLIND_DOOR_CONN_LEFT_OFFSET -> "Blind door connector left offset"
BLIND_DOOR_CONN_RIGHT_OFFSET -> "Blind door connector right offset"

BLIND_DOOR_OPERATION -> "Blind door operation: 1 -> DOWEL, 2 -> MINIFIX, 3 -> RAFIX, 4 -> SCREW, 5 -> CABINEO"
BLIND_DOOR_THICKNESS -> "Thickness of blind door (if equal nil or 0, blind door thickness will be default panel thickness)"

BLIND_DOOR_LABEL -> "Label of blind door"
BLIND_DOOR_TAG -> "Tag of blind door"

BLIND_DOOR_LEFT_EDGESTRIP_MAT -> "Left edgestrip material for blind door"
BLIND_DOOR_RIGHT_EDGESTRIP_MAT -> "Right edgestrip material for blind door"
BLIND_DOOR_TOP_EDGESTRIP_MAT -> "Top edgestrip material for blind door"
BLIND_DOOR_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material for blind door"

; Extra Part Parameters
EXTRA_PART_LABEL -> "Label of extra part"
EXTRA_PART_TAG -> "Tag of extra part"

EXTRA_PART_LEFT_EDGESTRIP_MAT -> "Left edgestrip material for extra part"
EXTRA_PART_RIGHT_EDGESTRIP_MAT -> "Right edgestrip material for extra part"
EXTRA_PART_TOP_EDGESTRIP_MAT -> "Top edgestrip material for extra part"
EXTRA_PART_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material for extra part"

; Filler Parameters
FILLER_LABEL -> "Label of filler"
FILLER_TAG -> "Tag of filler"

FILLER_LEFT_EDGESTRIP_MAT -> "Material of left edgestrip"
FILLER_RIGHT_EDGESTRIP_MAT -> "Material of right edgestrip"
FILLER_TOP_EDGESTRIP_MAT -> "Material of top edgestrip"
FILLER_BOTTOM_EDGESTRIP_MAT -> "Material of bottom edgestrip"

;prefvars
(if (null AKK_DEFAULT_LOADED)
	(progn
		(_SETA 'IS_EXTRA_PART_AVAILABLE T)
		(_SETA 'IS_BLIND_DOOR_AVAILABLE T)
		(_SETA 'EXTRA_PART_WID 150)
		(_SETA 'IS_BLIND_DOOR_INSIDE_MODULE nil)
		(_SETA 'OVERLAPPING_DISTANCE 70)
		(_SETA 'ASSUME_DRAWER_AS_DOOR nil)
		(_SETA 'AKK_PANELS_SOURCE nil)
		
		;AKK fixed and adjustable shelf parameters
		(_SETA 'BLIND_CORNER_FIXED_SHELF_ELEV_LIST (_ 60))
		(_SETA 'BLIND_CORNER_ADJUSTABLE_SHELF_ELEV_LIST (_ 360 660 960 ))
		
		; Blind Door Parameters
		(_SETA 'BLIND_DOOR_CONN_LEFT_OFFSET 180)
		(_SETA 'BLIND_DOOR_CONN_RIGHT_OFFSET 80)
		
		(_SETA 'BLIND_DOOR_OPERATION 0)
		(_SETA 'BLIND_DOOR_THICKNESS 0)
		
		(_SETA 'BLIND_DOOR_LABEL nil)
		(_SETA 'BLIND_DOOR_TAG nil)

		(_SETA 'BLIND_DOOR_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'BLIND_DOOR_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'BLIND_DOOR_TOP_EDGESTRIP_MAT nil)
		(_SETA 'BLIND_DOOR_BOTTOM_EDGESTRIP_MAT nil)

		; Extra Part Parameters
		(_SETA 'EXTRA_PART_LABEL nil)
		(_SETA 'EXTRA_PART_TAG nil)

		(_SETA 'EXTRA_PART_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'EXTRA_PART_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'EXTRA_PART_TOP_EDGESTRIP_MAT nil)
		(_SETA 'EXTRA_PART_BOTTOM_EDGESTRIP_MAT nil)
		
		; Filler Parameters
		(_SETA 'FILLER_LABEL nil)
		(_SETA 'FILLER_TAG nil)

		(_SETA 'FILLER_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'FILLER_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'FILLER_TOP_EDGESTRIP_MAT nil)
		(_SETA 'FILLER_BOTTOM_EDGESTRIP_MAT nil)

		; CONTINGENCIES
		; BLIND_DOOR_OPERATION
		(if (null (member BLIND_DOOR_OPERATION (_ 1 2 3 4 5))) 
			(_FSET (_ 'BLIND_DOOR_OPERATION nil))
			(progn
				(cond
					((equal BLIND_DOOR_OPERATION 1)
						(_FSET (_ 'BLIND_DOOR_OPERATION "DOWEL"))
					)
					((equal BLIND_DOOR_OPERATION 2)
						(_FSET (_ 'BLIND_DOOR_OPERATION "MINIFIX"))
					)
					((equal BLIND_DOOR_OPERATION 3)
						(_FSET (_ 'BLIND_DOOR_OPERATION "RAFIX"))
					)
					((equal BLIND_DOOR_OPERATION 4)
						(_FSET (_ 'BLIND_DOOR_OPERATION "SCREW"))
					)
					(T
						(_FSET (_ 'BLIND_DOOR_OPERATION "CABINEO"))
					)
				)
			)
		)
		; IS_EXTRA_PART_AVAILABLE
		(if (null (member IS_EXTRA_PART_AVAILABLE (_ nil T))) (_FSET (_ 'IS_EXTRA_PART_AVAILABLE T)))
		; IS_BLIND_DOOR_INSIDE_MODULE
		(if (null (member IS_BLIND_DOOR_INSIDE_MODULE (_ nil T))) (_FSET (_ 'IS_BLIND_DOOR_INSIDE_MODULE T)))
		; ASSUME_DRAWER_AS_DOOR
		(if (null (member ASSUME_DRAWER_AS_DOOR (_ nil T))) (_FSET (_ 'ASSUME_DRAWER_AS_DOOR T)))
		; AKK_PANELS_SOURCE
		(if (null (member AKK_PANELS_SOURCE (_ "INSOURCE" "OUTSOURCE"))) (_FSET (_ 'AKK_PANELS_SOURCE nil)))
		
		(_FSET (_ 'AKK_DEFAULT_LOADED T))
	)
)

; Standard Values, IS_EXTRA_PART_AVAILABLE nil, EXTRA_PART_WID 197, IS_BLIND_DOOR_INSIDE_MODULE nil, OVERLAPPING_DISTANCE 177, ASSUME_DRAWER_AS_DOOR nil, AKK_PANELS_SOURCE nil
; PRELIMINARY CONTROLS
(if (null AKK_PRELIMINARY_CONTROLS)
	(progn
		; IMPORTANT INFORMATION
		;  AK_DOOR_GAPS_DATA is a global variable which will be used in AKK_MANUFACTURER
		(cond
			((equal __MODULTYPE "AK1")
				; BLIND CORNER
				(_FSET (_ 'BLIND_CORNER_CONTROL T))
				; This global variable used in AKK_MANUFACTURER
				(_FSET (_ 'BCU_DOOR_GAPS_DATA (_ __DOORSBASETOPCLEAR __DOORSBASEHBETWEENCLEAR __DOORSBASEBOTTOMCLEAR)))
			)
			((equal __MODULTYPE "UK1")
				; WALL BLIND CORNER
				(_FSET (_ 'BLIND_CORNER_CONTROL T))
				; This global variable used in AKK_MANUFACTURER
				(_FSET (_ 'BCU_DOOR_GAPS_DATA (_ __DOORSWALLTOPCLEAR __DOORSWALLHBETWEENCLEAR __DOORSWALLBOTTOMCLEAR)))
			)
			(T
				(_FSET (_ 'BLIND_CORNER_CONTROL nil))
				(_FSET (_ 'BCU_DOOR_GAPS_DATA nil))
			)
		)

		; If module type is valid then preliminary controls are performed
		(if (_NOTNULL BLIND_CORNER_CONTROL)
			(progn
				(_RUNDEFAULTHELPERRCP "DOORS_DEFAULT" nil "p2chelper")
				(_RRCP "DEFAULT_HELPERS\\(DEFAULT)\\AKK_MANUFACTURER" nil)
				
				(_FSET (_ 'currentDoorID (_S2V (_& (_ "__DOOR" AKK_MANU_CUR_DOOR_INDEX "_DOORID")))))
				(_FSET (_ 'currentDoorTYPE (_S2V (_& (_ "__DOOR" AKK_MANU_CUR_DOOR_INDEX "_TYPE")))))

				(if (_NOTNULL currentDoorID)
					(progn
						(_FSET (_ 'currentDoorMODEL (_GETDOORMODEL currentDoorID)))			
						
						; Extra Item code is set in here
						(_FSET (_ 'extraItemCODE (_& (_ currentDoorMODEL "_" (_CONVERTDOORTYPETOCOSTTYPE currentDoorTYPE)))))
						
						; MATERIAL CONTROL
						(_FSET (_ 'currentDoorMAT (_GETDOORMATS currentDoorID GENERAL_DOORS_LAYER)))
					)
				)
				
				(if (null currentDoorMAT)
					(_FSET (_ 'currentDoorMAT (_GETLAYERMAT GENERAL_DOORS_LAYER)))
				)
											
				; SOURCE CONTROL
				(if (_NOTNULL AKK_PANELS_SOURCE)
					(_FSET (_ 'sourceOfPanels AKK_PANELS_SOURCE))
					(progn
						; In this case source of blind corner panels are not declared by user
						(if (_NOTNULL currentDoorID)
							(_FSET (_ 'sourceOfPanels (_GETDOORSOURCE currentDoorID)))
						)
						(if (null sourceOfPanels)
							(_FSET (_ 'sourceOfPanels "OUTSOURCE"))
						)
					)
				)
				; IMPORTANT INFORMATIONS
				; Four cases for assembling blind corner panels
				; CASE			EXTRA PART			BLIND DOOR MATERIAL			FILLER MATERIAL
				;  1				nil					 BODY						BODY
				;  2				 T					 BODY						DOOR
				;  3				nil					 DOOR						DOOR
				;  4				 T					 DOOR						DOOR
				
				; EXTRA_ITEM	It is the variable which is used to perform extra items of outsource panels which are made from door material
				;				When source of panels is OUTSOURCE and material mode of outsource panels are 2 then this variable gets T value, otherwise it gets nil
				
				; IOD 			It is the shortened version of IS OUTSOURCE DOOR. Variables which has this shortening FILLER_IOD, BLIND_DOOR_IOD and EXTRA_PART_IOD
				; 				These variables holds the value whether OUTSOURCE will be put as source tag or not
				(if (and (null IS_EXTRA_PART_AVAILABLE) (not (equal __BLINDDOORLAYER GENERAL_DOORS_LAYER)))
					(progn
						; This branch contains only CASE 1
						(_FSET (_ 'AKK_BLIND_DOOR_MAT __BLINDDOORMAT))
						(_FSET (_ 'AKK_FILLER_MAT __BLINDDOORMAT))
						
						(_FSET (_ 'AKK_BLIND_DOOR_GRAIN __BLINDDOORMATROT))
						(_FSET (_ 'AKK_FILLER_GRAIN __BLINDDOORMATROT))
						
						(_FSET (_ 'AKK_BLIND_DOOR_EDGES T))
						(_FSET (_ 'AKK_FILLER_EDGES T))
						
						; Whatever the source of panels is both part will be created from body material. Thats why there is no need for extra item
						(_FSET (_ 'BLIND_DOOR_EXTRA_ITEM nil))
						(_FSET (_ 'FILLER_EXTRA_ITEM nil))
						
						(_FSET (_ 'BLIND_DOOR_IOD nil))
						(_FSET (_ 'FILLER_IOD nil))
					)
					(progn
						(if (null __DOOR0_TYPE)
							(progn
								(_FSET (_ 'AKK_FILLER_GRAIN __BLINDDOORMATROT))
								(_FSET (_ 'AKK_FILLER_MAT __BLINDDOORMAT))	
							)
							(progn
								; This branch contains CASE 2, 3 and 4
								(_FSET (_ 'AKK_FILLER_MAT (_DERIVEDOORMAT sourceOfPanels currentDoorTYPE currentDoorMODEL currentDoorMAT OUTSOURCE_DOORS_MATERIAL_MODE)))
								(_FSET (_ 'AKK_FILLER_GRAIN (_GETDOORMATROTS currentDoorID GENERAL_DOORS_LAYER)))
							)
						)
							
						(if (equal sourceOfPanels "INSOURCE")
							(progn
								; INSOURCE
								(_FSET (_ 'AKK_FILLER_EDGES T))
								(_FSET (_ 'FILLER_EXTRA_ITEM nil))
								(_FSET (_ 'FILLER_IOD nil))
							)
							(progn
								; OUTSOURCE
								(_FSET (_ 'AKK_FILLER_EDGES nil))
								(if (equal OUTSOURCE_DOORS_MATERIAL_MODE 2)
									(_FSET (_ 'FILLER_EXTRA_ITEM extraItemCODE))
									(_FSET (_ 'FILLER_EXTRA_ITEM nil))
								)
								(_FSET (_ 'FILLER_IOD T))
							)
						)
						
						; EXTRA PART Controls
						(if (_NOTNULL IS_EXTRA_PART_AVAILABLE)
							(progn
								(_FSET (_ 'AKK_EXTRA_PART_WID AKK_MANU_EXTRA_PART_WID))
								(_FSET (_ 'AKK_EXTRA_PART_HEI AKK_MANU_EXTRA_PART_HEI))
								
								(_FSET (_ 'AKK_EXTRA_PART_MAT AKK_FILLER_MAT))
								(_FSET (_ 'AKK_EXTRA_PART_GRAIN AKK_FILLER_GRAIN))
								(_FSET (_ 'AKK_EXTRA_PART_EDGES AKK_FILLER_EDGES))
								
								(_FSET (_ 'EXTRA_PART_EXTRA_ITEM FILLER_EXTRA_ITEM))
								(_FSET (_ 'EXTRA_PART_IOD FILLER_IOD))
							)
						)
						; BLIND DOOR CONTROLS
						(if (equal __BLINDDOORLAYER GENERAL_DOORS_LAYER)
							(progn
								; DOOR
								(_FSET (_ 'AKK_BLIND_DOOR_MAT AKK_FILLER_MAT))
								(_FSET (_ 'AKK_BLIND_DOOR_GRAIN AKK_FILLER_GRAIN))
								(_FSET (_ 'AKK_BLIND_DOOR_EDGES AKK_FILLER_EDGES))
								
								(_FSET (_ 'BLIND_DOOR_EXTRA_ITEM FILLER_EXTRA_ITEM))
								(_FSET (_ 'BLIND_DOOR_IOD FILLER_IOD))
							)
							(progn
								; BODY
								(_FSET (_ 'AKK_BLIND_DOOR_MAT __BLINDDOORMAT))
								(_FSET (_ 'AKK_BLIND_DOOR_GRAIN __BLINDDOORMATROT))
								(_FSET (_ 'AKK_BLIND_DOOR_EDGES T))
								
								(_FSET (_ 'BLIND_DOOR_EXTRA_ITEM nil))
								(_FSET (_ 'BLIND_DOOR_IOD nil))
							)
						)
					)
				)
				; Global variables of blind corner blind door are determined
				(_FSET (_ 'AKK_BLIND_DOOR_HEI AKK_MANU_BLIND_DOOR_HEI))
				(_FSET (_ 'AKK_BLIND_DOOR_WID AKK_MANU_BLIND_DOOR_WID))
				; Global variables of blind corner filler are determined
				(_FSET (_ 'AKK_FILLER_WID AKK_MANU_FILLER_WID))
				(_FSET (_ 'AKK_FILLER_HEI AKK_MANU_FILLER_HEI))
				; AKK_PANELS_SOURCE is overloaded by sourceOfPanels
				(_FSET (_ 'AKK_PANELS_SOURCE sourceOfPanels))
			)
		)
		(_FSET (_ 'AKK_PRELIMINARY_CONTROLS T))
	)
)
(_NONOTCH)
