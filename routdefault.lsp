;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;; firmspc.lsp CIZIMCI.COM TARAFINDAN KULLANILDIĞI İÇİN kütüphane overwrite larının hepsini routdefault.lsp içerisinde yap!!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(if (null g_ExplodeCmd)
	(progn
		(command "_.UNDEFINE" "_EXPLODE")
		(setq g_ExplodeCmd T)
	)
)

(defun lmm_runMacrosInsideBlocks ( / cloneModul deleteSS drawEnt lmm-runMacro-Counter-1 tempCode)
	(if (equal lmm-s-runscriptsubrcps "1")
		(progn	
			(setvar "EXPERT" 2)
			(foreach cloneModul (convertSStoList (ssget "x" (list  (list -3 (list  lmm-scriptToRcp )) '(0 . "INSERT"))))
					(setq tempEntlast (entlast))
					(setq tempCode (getEntPro cloneModul 2))
					(command "_.INSERT" (getEntPro cloneModul 2) (getEntPro cloneModul 10) 1.0 1.0 "")			
					
					(command "_.EXPLODE" (entlast) )
					(setq lmm-runMacro-Counter-1 0)
					(setq deleteSS (collectMents tempEntlast))
					(if (> (sslength deleteSS) 0)
						(progn	
							(foreach lmm-moduleEntNameInLine (convertsstolist deleteSS)
								(if (lmm_existApp lmm-moduleEntNameInLine lmm-scriptToRcp )
								(progn
									(setq tempEnt (entlast))
									(lmm_runMacroByModuleCapsule lmm-moduleEntNameInLine (list nil))
									(setq lmm-runMacro-Counter-1 (1+ lmm-runMacro-Counter-1))
									
									(if (> (sslength (collectMents tempEnt) ) 0)
										(progn
											(foreach drawEnt (convertsstolist (collectMents tempEnt))
												(putStringMData drawEnt "LMM" (car lmm-k-parentmodul) tempCode)
											)
										)
									)
								)
								)
							)
							(COMMAND "_.DELETE" deleteSS "" )			
						)
					)
			)
			(setvar "EXPERT" 0)	
		)
	)
)


