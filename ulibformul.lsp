(defun cutTwoRailGroove (grooveWid grooveDept grooveFrontDist distBetweenGroove / cutTwoRailGroove_params  )
	(setq cutTwoRailGroove_params (getnth 0 (getRailParams)) )		
	(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cutTwoRailGroove_params) 
	
	(setq mozel_mUnit T 
		altkanal1pt  (replace (- ad_et grooveDept) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) ) 
		altkanal2pt  (replace (- ad_et grooveDept) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid grooveWid distBetweenGroove) ) )
		ustkanal1pt	 (replace (- dHeight ad_et (- ad_et grooveDept) ) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) )
		ustkanal2pt  (replace (- dHeight ad_et (- ad_et grooveDept)) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid grooveWid distBetweenGroove ) ))
	)
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle altkanal1pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle altkanal2pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0(+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    grooveWid 0.0 dAngle ustkanal1pt "railGroove" "" nil "PANELPCONN")
	
	(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    grooveWid 0.0 dAngle ustkanal2pt "railGroove" "" nil "PANELPCONN")
	
	(princ)


)

(defun atla_ray (code note / kanalen kanalderinlik kanalonmesafe kanalarasi )
	;cutTwoRailGroove geniş
	(setq kanalen 1.4 kanalderinlik 1.1  kanalonmesafe 4.0 kanalarasi 1.0)
	(cutTwoRailGroove  kanalen kanalderinlik kanalonmesafe kanalarasi )
)

(defun cagberk_skm40 (code note  /  cagberk_skm40_params cagberk_skm40_params newPanelData tempZ)
		(setq cagberk_skm40_params (getnth 0 (getRailParams)) )		
		(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cagberk_skm40_params) 
		
		(setq mozel_mUnit T 
			altkanal1pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.9 ) ) 
			altkanal2pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
			ustkanal1pt	 (replace (- dHeight ad_et (- ad_et 1.2) ) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.25 ) )
			ustkanal2pt  (replace (- dHeight ad_et (- ad_et 1.2)) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
		)
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    0.8 0.0 dAngle altkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) 0.8 0.0 dAngle altkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0(+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(princ)
)

;standart raf
(defun xmf_raf_offset (yuk y inputet) 
	(+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet)
)

(defun xmf_raf_offset_list (yuk y inputet / xmf_raf_offset_result) 
	(repeat y
		(setq xmf_raf_offset_result (cons (+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet) xmf_raf_offset_result))
	)
	xmf_raf_offset_result
)

;Bitiş modülü raf araları eşitlemek için xmf_raf kullan
;x:Bu raf dolap içinde kaçıncı 
;y:Bu dolap içinde kaç raf var
;x ve y reelsayı olmalı 1.0 gibi 
(defun GM1_Raf (x y ) 
	(+ (* (- curDivH (* ad_et y)) (/ x (+ y 1.0)))  (* (- x 1.0) ad_et))
)

;Standar Cam raf
(defun GM1_CRaf (x y ) 
	(+ (*  curDivH (/ x (+ y 1.0)))  (* (- x 1.0) 0.8))
)

;Aventos içi iki raf olursa üsteki rafın dolabın üst içinden mesafesi. Örnek HF 040 {H2} 
(defun avn_ust_raf ()  
	(list (* curDivH 0.31))
)

;Aventos içi iki raf olursa alttaki rafın dolabın üst içinden mesafesi 
(defun avn_alt_raf ()
	(list (* curDivH 0.6431))
)

;_______
;Tek bölümlü dolabın bölüm yüksekliği
(defun bh_1bd ()
	 remCTH
)

;Tek bölümlü dolabın kapak yüksekliği
(defun kh_1bd ()
	 (- unitH ZTop ZBot)
)

;________
;4 eşit çekmeceli dolabın 1. bölüm yüksekliği
(defun bh_4s_#1c ()
	(+ (- (kh_4s_#1c) ad_et) ZTop (* ZBtwn 0.5))
)

;4 eşit çekmeceli dolabın 1. kapak yüksekliği
(defun kh_4s_#1c ()
	(* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.25)
)

;Dörde bölümlü sistemin 1 çekmeceli 1 kapaklı dolabın 3de 4e tekabül eden alt bölüm yüksekliği
(defun bh_4s_2+3+4#2b ()
	(- (kh_4s_2+3+4#2b) (- ad_et ZBot (* ZBtwn 0.5)))
)

;Dörde bölümlü sistemin 1 çekmeceli 1 kapaklı dolabın 3de 4e tekabül eden alt bölüm kapak yüksekliği
(defun kh_4s_2+3+4#2b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.75)  (* 2.0 ZBtwn))
)

;_________
;Dörte bölümlü sistemin 2de 4e tekabül eden bölüm yüksekliği
(defun bh_4s_1+2_#1b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZTop (* ZBtwn 0.5))
)

;Dörte bölümlü sistemin 2de 4e tekabül eden bölümün kapak yüksekliği
(defun kh_4s_1+2_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;Dörte bölümlü sistemin 2de 4e tekabül eden bölümün alt bölüm yüksekliği
(defun bh_4s_3+4_#2b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZBot (* ZBtwn 0.5))
)

;Dörte bölümlü 3 çekmeceli dolap orta bölme 

(defun bh_4s_#2b ()
	(+ (kh_4s_#1c) ZBtwn)
)
;_____

;3 çekmeceli 1 dar iki eşit çekmeceli dolap orta bölme yüksekliği
(defun bh_4s_3:8b_#1b ()
	(+ (kh_4s_3:8b_#1b) ZBtwn)
)

;3 çekmeceli 1 dar iki eşit çekmeceli dolap orta bölme kapak yüksekliği
(defun kh_4s_3:8b_#1b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.375) (* ZBtwn 0.5))
)

;3 çekmeceli 1 dar iki eşit çekmeceli dolap alt bölme yüksekliği 

(defun bh_4s_3:8b_#2b ()
	(+ (- (kh_4s_3:8b_#1b) ad_et) ZBot (* ZBtwn 0.50))
)

;_______
;Dörde bölümlü sistemin 4 eşit çekmeceli üçüncü bölüme tekabül eden bölüm yüksekliği, kapak birinci bölümdeki gibidir.
(defun bh_4s_#3c ()
	(+ (kh_4s_#1c) ZBtwn)
)

;Dörde bölümlü sistemin 4 eşit çekmeceli dördüncü bölüme tekabül eden bölüm yüksekliği, kapak birinci bölümdeki gibidir.
(defun bh_4s_#4c ()
	(- (kh_4s_#1c) (- ad_et ZBot (* ZBtwn 0.5)))
)

;Ankastre Fırın alt kapak
(defun ANK_K ()
	(+ remCTH (- (* 1.5 ad_et) ZBot (* ZBtwn 0.5) ) )
)

;____
;Üst kalkar kapaklı dolap üst bölüm 
(defun bh_2b_ust_#1b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZTop (* ZBtwn 0.5)) (* ad_et 0.5))
)

;Üst kalkar kapaklı dolap kapağı 
(defun kh_2b_ust_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;Üst kalkar kapaklı dolap alt bölüm 
(defun bh_2b_ust_#2b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZBot (* ZBtwn 0.5)) (* ad_et 0.5))
)

;____--______
;Asp. dolabı kapağı
(defun kh_1bd_asp ()
	(+ (- unitH ZTop ZBot) 13.9)
)

;_________
;Alt dolap sabit modüller için raf ayarı
(defun xmf_SabitAlt_Raf_Kural (inputet / temph)
	(setq temph (- ad_adh ad_et ad_et))
	(cond ((> ad_ADH 74.0)
		(list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
			((> ad_ADH 70.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil
		)
	)
)

;Üst dolap sabit modüller için raf ayarı
(defun xmf_SabitUst_Raf_Kural (inputUDH inputet / temph)
	(setq temph (- inputUDH ad_et ad_et))
	(cond ((> inputUDH 95.0)
		(list (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet)))
			((> inputUDH 71.0) (list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
			((> inputUDH 39.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil
		)
	)
)

(defun gm1_Ust_Raf_Kural (inputUDH inputet / tempH) 
	(setq temph (- inputUDH ad_et ad_et))
	(cond ((> inputUDH 95.0)
		(gm1_createShelfDividList tempH inputet 3.0))
			((> inputUDH 71.0) (gm1_createShelfDividList tempH inputet 2.0))
			((> inputUDH 39.0) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil
		)
	)
)

(defun gm1_Alt_Raf_Kural (inputad_ADH inputet / tempH) 
	(setq temph (- inputad_ADH ad_et ad_et))
	(cond ((> inputad_ADH 120.0)
		(gm1_createShelfDividList tempH inputet 3.0))
			((> inputad_ADH 75.0) (gm1_createShelfDividList tempH inputet 2.0))
			((> inputad_ADH 55.0) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil
		)
	)
)

;(defun gm1_Alt_Raf_Kural ( / tempH)
;	(setq temph (-	ad_ADH ad_et ad_et))
;	(cond ((> ad_ADH 80.0)
;		(gm1_createShelfDividList temph ad_et 2.0))
;		((> ad_ADH 77.0) (gm1_createShelfDividList temph ad_et 1.0))
;		(T nil
;		)
;	)
;)

;Üstyarım dolap yüksekliği UDH2
(defun UYRM ()
	(/ ad_UDH1 2.0)
)

(defun UYRM2 ()
	(/ ad_UDH2 2.0)
)

;Üstyarım dolap yüksekliği UDH3
(defun UYRM3 ()
	(/ ad_UDH3 2.0)
)

;Üstyarım dolap yüksekliği UDH4
(defun UYRM4 ()
	(/ ad_UDH4 2.0)
)

;Üstyarım dolap yüksekliği UDH5
(defun UYRM5 ()
	(/ ad_UDH5 2.0)
)

;Buzdolabı kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;Buzdolabı kabini BDH3
(defun BCH3 ()
	(+ ad_BDH3 ad_bazaH)
)

;Buzdolabı kabini BDH4
(defun BCH4 ()
	(+ ad_BDH4 ad_bazaH)
)

;Buzdolabı kabini BDH5
(defun BCH5 ()
	(+ ad_BDH5 ad_bazaH)
)

;Set üstü dolap ölçüsü
(defun SETUSTU ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH)
)

;Set üstü dolap ölçüsü UDH2 
(defun SETUSTU2 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH2)
)

;Set üstü dolap ölçüsü UDH3 
(defun SETUSTU3 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH3)
)

;Set üstü dolap ölçüsü UDH4 
(defun SETUSTU4 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH4)
)

;Set üstü dolap ölçüsü UDH5 
(defun SETUSTU5 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH5)
)

;____________
;Boy dolap 
(defun bh_boy_#1b ()
	(- (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5)))  g_ClearWallZBot )
)

(defun kh_boy_#1b ()
	;(+ (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	;(- (- (+ (bh_boy_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5)) (* (- g_ClearWallZBot g_ClearWallZBtwn) 0.5))
	(- (+ (bh_boy_#1b) (* ad_et 1.5) ) ZTop (* Zbtwn 0.5))
)

(defun bh_boy_#2b ()
	(+ (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5) ) g_ClearWallZBot )
)

(defun kh_boy_#2b ()
	;(- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot)
	(- (+ (bh_boy_#2b) (* ad_et 1.5) ) ZBot (* Zbtwn 0.5))
)

;________
;Boy dolap Üst uzun kapaklı üst bölüm Alt bölüm alt modül kapağı gibi çalışır
(defun bh_boy2_#1b ()
	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- g_ClearBaseZTop g_ClearWallZBtwn))
)
(defun kh_boy2_#1b ()
;	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	(- (+ (bh_boy2_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5))
)

(defun bh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5) ) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

(defun kh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

;Boy Fırın modülü iki çekmeceli çekmece bölümü
;üst çekmece
(defun bh_bf2c_#2b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBot) 0.50) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf2c_#2b) ZBtwn) (* ad_et 0.5)) 
)

(defun kh_bf2c_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.5))
)

;alt çekmece
(defun bh_bf2c_#3b ()
	(- (+ (kh_bf2c_#2b) (* ZBtwn 0.5) ZBot) ad_et)
)

;Boydolap üç çekemeceli modülüm dar çekmecesi kapak boşluk tölarasları kapağa yedirilmi ve kapaklara paylaştırılmıştır.
(defun bh_bf3c_#1b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf3c_#1b) ZBtwn) (* ad_et 0.5))
)

(defun bh_bf3c_#2b ()	
	 (+ (kh_bf3c_#1b) ZBtwn) 
)

(defun kh_bf3c_#1b ()
	(- (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.25))
)

;Boydolap üç çekemeceli modülüm alt büyük çekmecesi kapak boşluk tölarasları kapağa yedirilmi ve kapaklara paylaştırılmıştır.
(defun bh_bf3c_#3b ()
	(- (+ (kh_bf3c_#3b) (* ZBtwn 0.5) Zbot)  ad_et )
)
(defun kh_bf3c_#3b ()
	(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.50) ZBtwn) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Boy Fırın Mikrodalgalı modül üst kapak bölüm
(defun bh_bfmd_#1b ()
	 (- remCTH (bh_bfmd_#2b)) 
)

(defun kh_bfmd_#1b ()
	(+ (- ( bh_bfmd_#1b ) Ztop (* ZBtwn 0.5) ) (* ad_et 1.5))
)

;Boy Fırın Mikrodalga alt sabit çekmeceli
(defun bh_bfmd_#2b ()
	(- (+ (kh_bfmd_#2b) (* ZBtwn 0.5) Zbot) (* ad_et 1.5))
)

(defun kh_bfmd_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Buzdolabı kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;________
;Yarımboy dolap yüksekliği
(defun YBOY ()
	(- ad_UDY ad_bazaH)
)

;Yarımboy dolap içi 
(defun gm1_YBOY_RAF ()
	(gm1_createShelfDividList curDivH ad_et 3.0)
)

;Yarımboy tek kapaklı dolap
(defun bh_yboy ()
	 remCTH
)
(defun kh_yboy ()
	 (- H ZTop ZBot)
)

;_______
;Yarımboy Fırın Mikrodalga alt sabit çekmeceli
;Üst bölüm sabit parca 
(defun bh_ybd_#1b ()
	(- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5)))
)

(defun kh_ybd_#1b ()
	(+ (- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et Ztop) )
)

(defun bh_ybd_#2b ()
	(- (* ad_ADH 0.25) (* ZBtwn 0.5) (* ad_et 1.5))
)

(defun kh_ybd_#2b ()
	(- (- (* ad_ADH 0.25) (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBot)
)

;_______bardaklıklık______
(defun Ust_Brdk_b1 (inputUDH / temph)
	(setq temph (- inputUDH ad_et ad_et ad_et 27.15))
)
	
(defun Ust_Brdk_k1 (inputUDH / temph)
	(setq temph (- (- inputUDH (* ad_et 0.5) ad_et 27.15) (* ZBtwn 0.5) Ztop))
)

; GOLA KÜTÜPHANESİ
;Tek bölümlü dolabın kapak yüksekliği
(defun kh_1bdGl ()
	 (- unitH ZTop ZBot 3.2)
)

;Dörte bölümlü sistemin 2de 4e tekabül eden bölümün kapak yüksekliği
(defun kh_4s_1+2_#1bGl ()
	(- (kh_4s_1+2_#1b) 3.2)
)

;4 eşit çekmeceli dolabın 1. kapak yüksekliği
(defun kh_4s_#1cGl ()
	(+ (kh_4s_#1c) (GolaUstAra) 0.1)
)

;kapak üst boşluk
(defun GolaUstAra ()
	-1.7
)

;İki çekmece Alt ara boşluk
(defun 2CekGolaAra ()
	-3.35
)

;İki çekmece Alt ara boşluk
(defun 3CekGolaAra ()
	-1.75
)

; GENEL KENAR BANDI İSMİ
(setq GenelPvcKodu "Pvc_Genel")
; GENEL KAPAK BANDI İSMİ
(setq KapakPvcKodu "Pvc-1.0mm")